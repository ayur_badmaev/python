# Список дел с удалением, редактированием, добавлением задач.

from tkinter import *


root = Tk() #корневой виджет



def paint_interface(): # эта функция будет рисовать интерфейс
	
	global texts #global для того, чтобы я мог обращаться к данным объектам вне этой функции, то есть в функциях кнопок
	texts = []
	global text_field  #global для того, чтобы я мог обращаться к данному объекту вне этой функции, то есть в функциях кнопок
	text_field = Entry(root,width=40, font="Verdana 12") #поле ввода текста (объект)
	for i in range(0,get_number_of_deal()):
		texts+=[Entry(root,width=40, font="Verdana 12")]


	root.title("приложение список задач") 
	button_add_to_do.pack(fill=BOTH)
	text_field.pack(fill=BOTH)
	button_clear.pack(fill=BOTH)

	for i in range(0,len(texts)):
		
		texts[i].pack(fill=BOTH)
		texts[i].insert(END,get_text(i))
		
	button_save.pack(fill=BOTH)

	root.mainloop()  # запускает циклический процесс отображения окна


def get_text(number): # получает задачу под номером number из файла
	with open('Список дел.txt', 'r') as list_to_do_read:         # открытие файла изменил везде, теперь не нужно заботиться о закрытии и исключениях
		string = list_to_do_read.read()
	number_start_point=0
	number_end_point =0
	

	i=0
	for k in range(0,len(string)):
		if k+len(deal_end) <= len(string) and string[k:k+len(deal_end)] == deal_end:
			if i==number:
				number_end_point = k
				break
			else:
				number_start_point = k+len(deal_end)
			i=i+1
	#list_to_do_read.close() уже не требуется закрытие
	return string[number_start_point:number_end_point]			


def get_number_of_deal(): # считает число дел
	with open('Список дел.txt', 'r') as list_to_do_read:         # открытие файла изменил везде, теперь не нужно заботиться о закрытии и исключениях
		string = str(list_to_do_read.read())
	i=0
	for k in range(0,len(string)):
		if k+len(deal_end) <= len(string) and string[k:k+len(deal_end)] == deal_end:
			i=i+1
	return i



def button_add_to_do_clicked(): ## здесь пишем то, что делает наша кнопка

	with open('Список дел.txt', 'a') as list_to_do_add: # когда только начал писать, думал сделать глобальной, но в итоге отказался, а комментарий не убрал
		string_of_field = text_field.get() ## получает текст из поля
		print(string_of_field+deal_end, file=list_to_do_add,end='') 
	for i in range(0, int(len(texts))):
		texts[i].pack_forget()
		
	text_field.pack_forget()
	button_clear.pack_forget()
	button_save.pack_forget()
	paint_interface()



def del_empty():        # да, так понятнее будет)
	with open('Список дел.txt', 'w') as list_to_do_write: # открываем на перезапись
		for i in texts:
			if i.get() !="":
				print(i.get()+deal_end, file=list_to_do_write,end='')

	for i in range(0, len(texts)):
		texts[i].pack_forget()
	text_field.pack_forget()
	button_clear.pack_forget()
	button_save.pack_forget()

	paint_interface()


def save_changes():
	with open('Список дел.txt', 'w') as list_to_do_write: # открываем на перезапись
		for i in texts:
			print(i.get()+deal_end, file=list_to_do_write,end='')

	for i in range(0, len(texts)):
		texts[i].pack_forget()
	text_field.pack_forget()
	button_clear.pack_forget()
	button_save.pack_forget()

	paint_interface()


button_add_to_do = Button(root, text="добавить задачу",command=button_add_to_do_clicked) #кнопка добавить задачу
button_clear = Button(root, text="удалить пустые",command=del_empty) # кнопка удаления пустых
button_save = Button(root, text="сохранить изменения", command=save_changes) #кнопка сохранения изменений

deal_end='\n' 

'''
end of deal вначале сделал с расчётом на многострочные задачи, 
но так как в итоге я позволил делать только однострочные,
маркером теперь является переход на следующую строку
'''

def main():
	with open('Список дел.txt', 'a'):  #создаём файл для задач, если такого нет
		pass
	paint_interface()
	
main()